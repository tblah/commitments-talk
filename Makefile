NAME=presentation
BIBFILE=$(NAME).bib
PDF=$(NAME).pdf
TEX=$(NAME).tex

JUNK=*.aux *.bbl *.bcf *.blg *.log *.out *.run.xml *.synctex.gz *.nav *.snm *.toc _minted-presentation *.vrb

LATEX=xelatex
LATEX_ARGS=-shell-escape
BIBER=biber
RM=rm -rf
VIEWER=evince

$(PDF): $(TEX) $(BIBFILE)
	$(LATEX) $(LATEX_ARGS) $(TEX)
	$(BIBER) $(NAME)
	$(LATEX) $(LATEX_ARGS) $(TEX)

.PHONY: view
view: $(PDF)
	$(VIEWER) $(PDF)

.PHONY: clean
clean:
	$(RM) $(PDF) $(JUNK); true
