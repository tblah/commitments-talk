\documentclass{beamer}
\usetheme{Boadilla}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}

\usepackage{euler}
\usepackage{subcaption}
\usepackage{caption}
\usepackage[section]{placeins}
\usepackage{hyperref}
\usepackage[autostyle]{csquotes}
\MakeOuterQuote{"}

%\usepackage[backend=biber,sorting=none,citestyle=alphabetic-verb]{biblatex}
\usepackage[backend=biber,sorting=none,citestyle=authoryear]{biblatex}
\addbibresource{presentation.bib}

% make the bibliography use a small font
\renewcommand*{\bibfont}{\scriptsize}

\usepackage{minted}

\title{Some Slightly Obscure Cryptography}
\subtitle{Pedersen Commitments \& Homomorphic Cryptography}
\author{Tom Eccles \and IRC: \texttt{ecclescake}}

\begin{document}
\begin{frame}
    \titlepage
\end{frame}
\begin{frame}
    \frametitle{Outline}
    \tableofcontents
\end{frame}

\section{Introduction}
\begin{frame}
    \textbf{Introduction}
\end{frame}

\subsection{Commitment Schemes}
\begin{frame}
    \frametitle{Commitment Schemes}
% Commitments are often used to claim knowledge of some data without
% revealing the data. For example, in a puzzle competition, a participant may
% publish a commitment to the solution of a puzzle so that if challenged at
% a later time, the participant can prove that they knew the solution of the
% puzzle at the time at which they published the commitment. Doing so will
% not reveal the solution to the puzzle to other participants until the time at
% which the commitment was opened.
%
% E.g. publish H( R||C ) (computationally binding, information theoretically concealing)
    \begin{itemize}
        \item A commitment is computed on some data.
        \item Publishing a commitment to the data does not reveal the data itself.
        \item A commitment can be opened by publishing the data and some key.
        \item The commitment is \textit{binding}: once opened, any observer can verify that the commitment could only have been made to that data.
        \item This is useful for proving knowledge of something.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Commitment Scheme Properties}
    Commitment schemes should be \textit{binding} and \textit{concealing}. Ideally these properties are \textit{information theoretically} secure. Sometimes they are only \textit{computationally} secure [\cite{smart03_crypt}].
    \begin{itemize}
        \item \textbf{Binding} - No adversary can output two equal commitments to different data with computation less than a brute-force search.
        \item \textbf{Concealing} - The adversary chooses two equal length messages, a challenger outputs a commitment to one of the messages (chosen randomly), the adversary cannot tell (better than chance) which of the messages were committed to.
        \item \textbf{Information Theoretic Security} - The adversary is computationally unbounded.
        \item \textbf{Computational Security} - The adversary is computationally bounded (the proof depends upon the algorithmic complexity of the problem - there is no proof that a better algorithm does not exist (or a quantum computer!))
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Commitment Scheme Properties 2}
    \textit{Lemma:} No commitment scheme is both \textit{information theoretically concealing} and \textit{binding}.
    \\\\
    \textit{Proof:} To be \textit{information theoretically binding} the commitment function must be one-to-one. However one-to-one functions are invertible and so depend upon the complexity of this inversion [\cite{smart03_crypt}].
\end{frame}

\subsection{Homomorphic Encryption}
\begin{frame}
    \frametitle{Homomorphic Encryption}
    \begin{itemize}
        \item This is when you can perform computations on a ciphertext
        \item Such as adding or multiplying two ciphertexts:
        \item $E_k(p_1) \oplus E_k(p_2) = E_k(p_1 + p_2)$
        \item Not all cryptosystems have this property but many (unpadded RSA, ECDSA, ...) do [\cite{wikipedia_homomorphic_encryption}] - this emphasises the important difference between confidentiality and authenticity.
    \end{itemize}
\end{frame}

\section{Crash-course in some maths}
\begin{frame}
    \textbf{Maths}
\end{frame}

%\subsection{Background on Finite Abelian Groups}
%\begin{frame}
%    \frametitle{Albelian Groups}
%    Group is a set $\mathcal{S}$ together with an operation $\cdot$ which conform to the following rules (think about it like defining an API)
%
%    For all $a, b, c$ in $\mathcal{S}$:
%    \begin{itemize}
%        \item \textbf{Closure} - $a\cdot b \in \mathcal{S}$
%        \item \textbf{Associativity} - $(a\cdot b)\cdot b \implies a \cdot (b \cdot c)$
%        \item \textbf{Identity Element} - $\exists e \in \mathcal{S} \mathrm{such that} e \cdot a = a \cdot e = a$
%        \item \textbf{Inverse Element} - $\forall a \exits a^{-1} \: \mathrm{such that} \: a \cdot a^{-1} = a^{-1} \cdot a = e$
%    \end{itemize}
%
%    Additionally for Abelian Groups:
%    \begin{itemize}
%        \item \textbf{Commutivity} - $a \cdot b = b \cdot a$
%    \end{itemize}
%\end{frame}

\subsection{A Quick Introduction to Modular Arithmetic}
\begin{frame}[fragile]{}
    \frametitle{A Quick Introduction to Modular Arithmetic}
    \begin{itemize}
        \item Modular arithmetic is defined over a finite set of integers. Arithmetic overflow is wrapping.
        \item "Integers modulo $n$" means the set of integers $\{0, 1, ..., n-1\}$
        \item Informally this (for positive integers) is the same as doing the arithmetic normally and then using C's \texttt{\%} operator with n.
        \item For example: 
    \end{itemize}

    \begin{minted}[breaklines]{c}
unsigned m = 10;
unsigned n = 5;
// calculation modulo 12
unsigned k = (3*n + m*n) % 12;
assert(k == 5);
    \end{minted}
\end{frame}

\subsection{The Discrete Logarithm Problem}
\begin{frame}
    \frametitle{The Discrete Logarithm Problem}
    \begin{itemize}
        \item $\log_b(a) = k \leftrightarrow b^k = a$
        \item \textbf{Discrete} logarithms are logarithms restricted to only integers
        \item For example $\log_3(9) = 2 \Leftrightarrow 3^2 = 9$
        \item No known efficient algorithm can compute discrete logarithms using non-quantum computers. This makes it prohibitively difficult when the inputs are large enough\footnote{The (relatively new) "Logjam" pre-computation attack puts 1024-bit discrete logarithms within the capabilities of nation state-scale attackers [\cite{logjam}].}.
    \end{itemize}
\end{frame}

\section{Pedersen Commitments}
\begin{frame}
    \textbf{Pedersen Commitments}
\end{frame}

\subsection{Properties}
\begin{frame}
    \frametitle{Properties}
        Pedersen's scheme [\cite{pedersenil_non_inter_infor_theor_secur}] provides homomorphic commitments.
    \begin{itemize}
        \item The commitments are \textit{computationally binding}
        \item The commitments are \textit{information theoretically concealing}
        \item The commitments are homomorphic over addition
        \item The commitments are homomorphic over multiplication
    \end{itemize}
\end{frame}

\subsection{Definition}
\begin{frame}
    \frametitle{Definition}
    \begin{itemize}
        \item $p$ is a large (2048-4096 bit) prime
        \item $g, h$ are integers modulo $p$ \footnote{In practice $g, h, x, r$ are taken from a cyclic subgroup of $\mathcal{Z}/p\mathcal{Z}$ for performance reasons}
        \item $x$ is the data to be committed to (treated as an integer modulo $p$)
        \item $r$ is an integer chosen uniformly at random from the set of integers modulo $p$
    \end{itemize}

    \[
        \mathrm{commit}_r(x) := h^xg^r (\mathrm{mod} \: p)
    \]

    This commitment can be opened by outputting $(x, r)$. The commitment is verified by checking that the equality holds.
\end{frame}

\subsection{Demonstrating Properties}
\begin{frame}
    \frametitle{Computationally Binding [\cite{smart03_crypt}]}
    \begin{itemize}
        \item Suppose Alice, after having committed to $c = \mathrm{commit}_a(x) = h^xg^a$ changes her mind, so as to commit to $y$ instead.
        \item She needs to compute $c/h^y$, then calculate the discrete logarithm of it to the base $g$.
        \item She can now open the commitment with $(b, y)$ instead of $(a, x)$.
        \item $c = h^xg^a = h^yg^b$
        \item $c/h^y = g^b$
        \item $\log_g(c/h^y) = b$
        \item Therefore Pedersen commitments are \textit{computationally binding} (the difficulty depends on the difficulty of calculating a discrete logarithm).
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Information Theoretically Concealing [\cite{smart03_crypt}]}
    \begin{itemize}
        \item Suppose Eve has a copy of $b = h^xg^a$ and wants to know the value of $x$ before the commitment is opened.
        \item For any given value of $b$ and every value of $x$, there is a value of $a$ which satisfies the equality.
        \item $a$ was chosen uniformly at random.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Additive Homomorphism} 
    \begin{itemize}
        \item $\mathrm{commit}_a(x) \oplus \mathrm{commit}_b(y) = $
        \item $h^xg^a \oplus h^yg^b =$
        \item $h^xg^a \cdot h^yg^b =$
        \item $h^xh^yg^ag^b =$
        \item $h^{x+y}g^{a+b} =$
        \item $\mathrm{commit}_{a+b}(x+y)$
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Multiplicative Homomorphism} 
    \begin{itemize}
        \item $\mathrm{commit}_a(x) \otimes y = $
        \item $\left(\mathrm{commit}_a(x)\right)^y = $
        \item $\left(h^xg^a\right)^y = $
        \item $h^{xy}g^{ab} =$
        \item $\mathrm{commit}_{ab}(xy)$
    \end{itemize}
\end{frame}

\section{Applications}
\begin{frame}
    \frametitle{Applications}
    \begin{itemize}
        \item Electronic voting
        \item Smart meter billing [\cite{eccles2017performance}] (future talk?)
        \item In general: zero-knowledge proofs for linear combinations
    \end{itemize}
\end{frame}

\section{References}
\begin{frame}
    \frametitle{References}
    \printbibliography
\end{frame}
\end{document}
